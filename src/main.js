import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueOnsen from 'vue-onsenui'
import VueSession from 'vue-session'
import VueCookies from 'vue-cookies'
import vueHeadful from 'vue-headful'
import VueSweetalert2 from 'vue-sweetalert2'
import App from './App.vue'
import Users from './User.vue'
import Story from './Story.vue'
import UserGuide from './UserGuide.vue'
import Gallery from './Gallery.vue'
import PageNotFound from './components/PageNotFound.vue'
import ExStories from './ExStories.vue'
import TestUpload from './TestUpload.vue'
import ExCompress from './ExCompress.vue'

Vue.use(VueRouter)
Vue.use(VueAxios, axios)
Vue.use(VueOnsen)
Vue.use(VueSession)
Vue.use(VueCookies)
Vue.component('vue-headful', vueHeadful)
Vue.use(VueSweetalert2)

const routes = [
  { path : '/aktivitas-kami/duniazuzhu/e-comic/explore/users', name: 'user', component: Users },
  { path : '/aktivitas-kami/duniazuzhu/e-comic/explore/', redirect: '/aktivitas-kami/duniazuzhu/e-comic/explore/gallery' },
  { path: '/aktivitas-kami/duniazuzhu/e-comic/explore/gallery', name: 'gallery', component: Gallery },
  { path: '/aktivitas-kami/duniazuzhu/e-comic/explore/story/:comic_seri', name: 'story', component: Story },
  { path: '/aktivitas-kami/duniazuzhu/e-comic/explore/user-guide', name: 'user-guide', component: UserGuide },
  { path: '/aktivitas-kami/duniazuzhu/e-comic/explore/example/stories', name: 'example-story', component: ExStories},
  { path: '*', component: PageNotFound },
  { path: '/aktivitas-kami/duniazuzhu/e-comic/explore/test-upload', name: 'test-upload', component: TestUpload },
  { path: '/aktivitas-kami/duniazuzhu/e-comic/explore/example/compress', name: 'example-compress', component: ExCompress }
];

const router = new VueRouter({
  routes,
  mode : 'history',
});

Vue.config.productionTip = false

// var CryptoJS = require("crypto-js");
// // Encrypt
// var ciphertext = CryptoJS.AES.encrypt('my message', 'secret key 123');
// console.log('encrypt: '+ciphertext);
// // Decrypt
// var bytes  = CryptoJS.AES.decrypt(ciphertext.toString(), 'secret key 123');
// var plaintext = bytes.toString(CryptoJS.enc.Utf8);
//
// console.log('plaintext: '+plaintext);

new Vue({
  el :'#app',
  router,
  render: h => h(App),
});
