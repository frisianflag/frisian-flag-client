//let baseUrlApi = 'https://recrowd.net/frisian-flag/api';
// let baseUrlApi = 'http://182.16.178.172/frisian-flag-dev/api';
let baseUrlApi = location.protocol + '//' + location.host + '/frisian-flag/api';
// let baseUrlApi = 'http://172.20.1.85:85/api';
const apiRoute = {
  'gallery' : baseUrlApi+'/user/comic/gallery',
  'story' : baseUrlApi+'/user/comic/details',
  'take_camera' : baseUrlApi+'/user/take/photo',
  'user_guide' : baseUrlApi+'/user/guide',
  'url_redirect' : baseUrlApi+'/url/redirect',
  'url_redirect_character' : baseUrlApi+'/url/redirect/character',
  'user_view' : baseUrlApi+'/user/view/comic',
};

export function apiRouter(){
  return apiRoute;
}
