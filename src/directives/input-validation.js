export function tagPrevention(event, field, type) {

    let input = event.target
    let a = field
    let isValid = true

    if (input.value.slice(-1) == '<' || input.value.slice(-1) == '>'){
        //a = input.value.slice(0, -1)
        isValid = false
    }

    if (input.value.includes("<")){
        //a = input.value.replace("<","")
        isValid = false
    }

    if (input.value.includes(">")){
        //a = input.value.replace(">","")
        isValid = false
    }

    if (!isValid){
        a = input.value.replace("<","")
        a = a.replace(">","")

        if (type==0){
            a = ''
        } 
    }

    return a
}

export function stringOnly(str){
    const regex = /(<([^>]+)>)/ig
    return str.replace(regex,"")
}

export function isEmailValid(input){
    const emailRegex = /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/
    return emailRegex.test(input)
}