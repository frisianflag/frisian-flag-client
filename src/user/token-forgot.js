var CryptoJS = require("crypto-js");
// var secret = 'monster-ar-token';

export function isLoggedInForgot() {
  const idToken = getIdTokenForgot();
  const expired_token = isTokenExpiredForgot();
  if('' != idToken && expired_token === 0){
    return 1;
  }else{
    return 0;
  }
}

export function getDataTokenForgot(){
  let token = localStorage.getItem('tokenTemp');
  if(token){
    var tokenEncrypted = window.atob(token)
    var bytes  = CryptoJS.AES.decrypt(tokenEncrypted, process.env.SECRET_KEY)
    // var bytes  = CryptoJS.AES.decrypt(token.toString(), secret);
    // var bytes  = CryptoJS.AES.decrypt(token.toString(), secret);
    var plaintext = bytes.toString(CryptoJS.enc.Utf8);
    token = JSON.parse(plaintext);
    // token = JSON.parse(token);
  }
  return token;
}

export function typeTokenForgot(){
  let token = getDataTokenForgot();
  let type_token = '';
  if(token){
    type_token = token.token_type;
  }
  return type_token;
}

export function getIdTokenForgot(){
    let token = getDataTokenForgot();
    let access_token = '';
    if(token){
      access_token = token.access_token;
    }
    return access_token;
}

export function isTokenExpiredForgot(){
  let token = getDataTokenForgot();
  let expired_token = '';
  let expired = 1;
  if(token){
    expired_token = token.expired_token;
    let d = new Date();
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    let year = d.getFullYear();
    let hours = d.getHours();
    let minutes = d.getMinutes();
    let seconds = d.getSeconds();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hours.length < 2) hours = '0' + hours;
    if (minutes.length < 2) minutes = '0' + minutes;
    if (seconds,length < 2) seconds = '0' + seconds;
    let date = year+'-'+month+'-'+day+' '+hours+':'+':'+minutes+':'+seconds;
    if(expired_token >= date){
      expired = 0;
    }

    return expired;
  }
}

export function removeTokenForgot(){
  localStorage.removeItem('tokenTemp');
}
