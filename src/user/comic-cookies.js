import Vue from 'vue'
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

let CryptoJS = require("crypto-js");
let sessionKeyRead = process.env.COMIC_COOKIES_NAME;
let sessionKeySpecial = process.env.SPECIAL_COOKIES_NAME;
// //===== get data cookie comic read  start ======
export function comicCookies(){
  let comicReadArray = [];
  let checkComicRead = Vue.cookies.isKey(sessionKeyRead);
  if(checkComicRead == true){
    let arrayString = Vue.cookies.get(sessionKeyRead);
    // Decrypt
    var bytes  = CryptoJS.AES.decrypt(arrayString.toString(), process.env.SECRET_KEY);
    var plaintext = bytes.toString(CryptoJS.enc.Utf8);
    let json = JSON.parse(plaintext);
    comicReadArray = json;
  }
  return comicReadArray;
}

export function specialEditionCookies(){
  let array = [];
  let checkComicSpecial = Vue.cookies.isKey(sessionKeySpecial);
  if(checkComicSpecial == true){
    let arrayString = Vue.cookies.get(sessionKeySpecial);
    let bytes = CryptoJS.AES.decrypt(arrayString.toString(), process.env.SECRET_KEY);
    let plaintext = bytes.toString(CryptoJS.enc.Utf8);
    let json = JSON.parse(plaintext);
    array = json;
  }
  return array;
}
