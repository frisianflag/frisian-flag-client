import axios from 'axios';
import { apiRouter } from './../api-routing.js';
var CryptoJS = require("crypto-js");

// var secret = 'monster-ar-token';

export function isLoggedIn() {
  const idToken = getIdToken();
  const expired_token = isTokenExpired();
  if('' != idToken && expired_token === 0){
    return 1;
  }else{
    removeToken();
    return 0;
  }
}

export function getDataToken(){
  let token = localStorage.getItem('tokenff');
  if(token){
    var tokenEncrypted = window.atob(token)
    var bytes  = CryptoJS.AES.decrypt(tokenEncrypted, process.env.SECRET_KEY);
    //var bytes  = CryptoJS.AES.decrypt(token.toString(), process.env.SECRET_KEY));
    var plaintext = bytes.toString(CryptoJS.enc.Utf8);
    token = JSON.parse(plaintext);
    // token = JSON.parse(token);
  }
  return token;
}

export function typeToken(){
  let token = getDataToken();
  let type_token = '';
  if(token){
    type_token = token.token_type;
  }
  return type_token;
}

export function getIdToken(){
    let token = getDataToken();
    let access_token = '';
    if(token){
      access_token = token.access_token;
    }
    return access_token;
}

export function isTokenExpired(){
  let token = getDataToken();
  let expired_token = '';
  let expired = 1;
  if(token){
    expired_token = token.expired_token;
    let d = new Date();
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    let year = d.getFullYear();
    let hours = d.getHours();
    let minutes = d.getMinutes();
    let seconds = d.getSeconds();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (hours.length < 2) hours = '0' + hours;
    if (minutes.length < 2) minutes = '0' + minutes;
    if (seconds,length < 2) seconds = '0' + seconds;
    let date = year+'-'+month+'-'+day+' '+hours+':'+':'+minutes+':'+seconds;
    if(expired_token >= date){
      expired = 0;
    }

    return expired;
  }

}

export function refreshToken(){
  let token = getDataToken();
  let refresh_token = '';
  if(token){
    refresh_token = token.refresh_token;
  }
  return refresh_token;
}

export function getRefreshToken(){
  let refresh_token = refreshToken();
  const formData = new FormData();
  formData.append('refresh_token', refresh_token);
  const header = {
      headers: {
        'Accept' :'application/json',
      }
  };

  axios.post(apiRouter().refresh_token, formData,header)
  .then( (response) => {
    let responseData = JSON.stringify(response.data.data);
    let ciphertext = CryptoJS.AES.encrypt(responseData, process.env.SECRET_KEY);
    localStorage.setItem('tokenff', window.btoa(ciphertext));
    // localStorage.setItem('tokenff', ciphertext);
      // localStorage.setItem('tokenff', JSON.stringify(response.data.data));
  })
  .catch((error) => {
    console.log(error);
  });
}

export function removeToken(){
  localStorage.removeItem('tokenff');
}
