const output = {
  globalObject: "this"
};

const publicPath = process.env.WEBPACK_PUBLIC_PATH || '/aktivitas-kami/duniazuzhu/e-comic/explore/';
const Dotenv = require('dotenv-webpack');

module.exports = {
  publicPath,
  configureWebpack: {
    devServer: {
      watchOptions: {
        poll: true,
      },
    },
    output,
    plugins: [
      new Dotenv()
    ]
  },
  chainWebpack: config => {
    config.module
      .rule('svg')
      .use('file-loader')
        .loader('vue-svg-loader')
  }
};
